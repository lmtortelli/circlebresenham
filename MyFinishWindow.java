/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package trabalho1;

import java.awt.event.*;

/**
* This class is for enabling the closing of a window.
*
* @author Frank Klawonn
* Last change 07.01.2005
*/
   public class MyFinishWindow extends WindowAdapter
   {
     public void windowClosing(WindowEvent e)
     {
       System.exit(0);
     }
   }


