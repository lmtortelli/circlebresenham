/*
 @author Lucas Tortelli
 @author Michel Pedroso
 */
package trabalho1;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
public class Trabalho1 extends Frame
{
    private int largura,altura;
    private int n;
    private int raio;
    private int x0;
    private int y0;
    private int secoes;
    private ArrayList<Point> listPoints;

  /**
  * Constructor
  */
  Trabalho1(int n, float raio, int x0, int y0, int secoes)
  {
    this.n = n;
    this.raio = (int)raio;
    this.x0 = x0;
    this.y0 = y0;
    this.secoes = secoes;
    this.setBackground(Color.WHITE);
    Dimension dimensao = Toolkit.getDefaultToolkit().getScreenSize();  
    largura = (int)dimensao.getWidth();
    altura = (int)dimensao.getHeight();
    this.setSize(largura,altura);
    addWindowListener(new MyFinishWindow());
    listPoints = new ArrayList<Point>();
    this.listPoints.clear();
  
  }
  Trabalho1(){
     addWindowListener(new MyFinishWindow());}
  
  private void circleDraw(Graphics g2d){
        int numeroDeRepeticoes=0;
        int x,y;
        int raio=this.raio;
        int secoes = 0;
        x = this.x0;
        y = this.y0;
        
       for(numeroDeRepeticoes=0;numeroDeRepeticoes<this.n;numeroDeRepeticoes++){
            if(numeroDeRepeticoes==0){
                this.circleBresenham(g2d, x, y,0);
                raio+=raio;
            }
            else{
                 x+=(2*this.raio);
                 this.circleBresenham(g2d, x, y,raio);
                 raio=raio+2*this.raio;
            }
        }

        Collections.sort(this.listPoints, new ComparaPontos());

  }

  //Os pontos passados por parâmetro é o ponto conhecido como o pico do circulo, ou seja y-raio
  private void circleBresenham(Graphics g,int xCenter, int yCenter, int raio){
      //PARAMETROS DO METODO
      // d e considerado e a estimacao do algoritmo de Bresenham
      // x,y servem para simular os pontos (0,0) do planto cartesiano, para o correto funcionamento do algoritmo
      // As variaveis diferencaX e diferencaY, servem para compensacao
      // x0 e y0 Sao os pontos passados por parametro, porém serve para poder altera-los, como pode ser visto no fim do algoritmo
      // xInf é a variável utilizada para controlar os desenhos quando as coodenadas são invertidas, ou seja, ela toma como ponto inicial o this.x0 e assim
      // Mantem todas as formas meio círculo no mesmo nível, somente variando o pi. É uma variável muito importante, sem elas as formas eram shifitadas para longe uma das outras, deixando um circulo em baixo do outro
      // 
      int d =1-this.raio;
      int x,y;
      int diferencaX, diferencaY;      
      int x0,y0;
      int xInf=this.x0;  
      // FIM DAS DECLARACOES
      
     
      //INICIALIZACAO DOS PARAMETROS
      // Diferencas servem para compensacao, inicialmente supõem-se que os pontos X0 e Y0 são iguais
      // x e y servem para simular o ponto inicial do cálculo do algoritmo de bresenham (x0,y0) = (0,R)
      // y0 é exatamente o ponto real na coordenada do JAVA 2D, como pode ser visto e this.y0 - raio, que indica a posicao exata no grafico
      // x0 é exatamento o ponto real do x na coordenada do JAVA 2D
      diferencaX = 0;
      diferencaY=0;
      x = 0;
      x0 = xCenter;
      y = this.raio;
      y0 = yCenter - this.raio;
      // FIM DAS INICIALIZACOES
      
      //Calculo das diferencas entre os pontos x0 e y0, como dito servirao para compensacao dos desenhos
      if(this.x0>this.y0){
          diferencaX = this.x0-this.y0;
      }
      else
          diferencaY = this.y0 - this.x0;
      //
      
      //Algoritmo de bresenham, o algoritmo para quando o Y simulado, chega ao X também simulado no plano coordenado real, ou seja, Y=X
      while(y>x){
          
          //Formula retirada do livro sobre o Algorítmo de Ponto Medio de Bresenham
          if(d<0){
              d+= 2*x+3;
          }
          else{
              d+= 2*(x-y) +5;
              //Variaveis de interpolacao da coordenada Y, note que tanto a variavel real quanto a simulada são interpoladas
              y0++;
              y--;
              
          }
          
          //Variaveis de interpolacao da coordenada X, note que tanto a real quando simulada são interpoladas, com adicao da xInf que será usada para
          // interpolar as linhas inferiores
          x0--;
          x++;
          xInf--;

          
          //--DESENHA LINHAS SUPERIORES--
          //Como nao existe coordenadas negativas, precisa-se relacionar o X0 e Y0 com a coordenada 0,0, então faz-e uma simulacao do ponto central
          //E faz o inverso dele, simplesmente somando os valores, deixando-o espelhado um do outro
          //Ver gráfico do círculo separado em OCTANTES!
          
          //g.drawLine(x0,y0, x0, y0);
          //g.drawLine(xCenter+x, y0, xCenter+x, y0);
          
//
          
          this.listPoints.add(new Point(x0,y0));
          this.listPoints.add(new Point(xCenter+x, y0));
          this.listPoints.add(new Point((y0+raio)-diferencaY+diferencaX,xInf+diferencaY-diferencaX));
          this.listPoints.add(new Point((yCenter+y+raio)-diferencaY+diferencaX, xInf+diferencaY-diferencaX));
          
          //--DESENHA LINHAS INFERIORES--
          //As diferencas servem para compensacao caso X0 e Y0 sejam diferentes, pois como n estamos trabalho com o plano cartesiano
          //Os pontos ao serem diferentes, nao encaixam, assim as compensacoes são necessarias
          //Outro fator importante, eh que as compensacoes devem ser feitas de modo contrario entre X e Y
          // Por exemplo, caso X0 > Y0, a difecerencaX tera um valor qualquer e a diferencaY será 0
          //Porém na diferenca X compensa-se nos locais de X adicionando a diferencaX e nos locais da coordenada Y retira-se a mesma diferenca
          // Assim o desenho se encaixa perfeitamente.
          
          //g.drawLine((y0+raio)-diferencaY+diferencaX,xInf+diferencaY-diferencaX, (y0+raio)-diferencaY+diferencaX, xInf+diferencaY-diferencaX);
          //g.drawLine((yCenter+y+raio)-diferencaY+diferencaX, xInf+diferencaY-diferencaX, (yCenter+y+raio)-diferencaY+diferencaX, xInf+diferencaY-diferencaX);
          
          //
      
      }
  }
  
public void sustain(long t)
  {
    long finish = (new Date()).getTime() + t;
    while( (new Date()).getTime() < finish ){}
  }
	
    public static double[] convexCombination(double[] a, double[] b, double alpha) {
        double[] result = new double[a.length];

        for (int i = 0; i < result.length; i++) {
            result[i] = (1 - alpha) * a[i] + alpha * b[i];
        }

        return (result);
    }
  
  private void drawingSquare(Graphics2D g2d){
      boolean flag = true;
      float div=this.listPoints.size()/(this.secoes*this.n);

      int control=0;
      int steps = this.secoes*this.n;
      double doublesteps = 15;
      
      
      Rectangle2D.Double square2 = new Rectangle2D.Double(0,0,10,10);
      //Graphics2D g2d = (Graphics2D) g;
      AffineTransform initialTransform = new AffineTransform();
      double[] initialMatrix = new double[6];
      initialTransform.getMatrix(initialMatrix);
      
      AffineTransform finalTransform = new AffineTransform();
      double[] finalMatrix = new double[6];
      finalTransform.getMatrix(finalMatrix);    
      
      //Declaracao dos SHAPES
      Shape initialShape = initialTransform.createTransformedShape(square2);
      Shape finalShape = finalTransform.createTransformedShape(square2);
      Shape s;
      AffineTransform intermediateTransform;
      int tam=0;
      int dif=0;
      if((this.secoes%2==0)||(this.secoes==1)){
          tam = this.n*this.secoes;
      }
      else
          tam = this.n*this.secoes+1;
         
      
      for(int i=0;i<tam;i++)
      {

        if(flag){
            int x=0;
            initialTransform.setToTranslation(this.listPoints.get(control).getX(), this.listPoints.get(control).getY());
            initialTransform.getMatrix(initialMatrix);
            if(control<this.listPoints.size()){
            x = this.listPoints.get(control).getX();}
            control+=div;
            if(control>=this.listPoints.size()){
                
                finalTransform.setToTranslation(this.listPoints.get(this.listPoints.size()-1).getX(), this.listPoints.get(this.listPoints.size()-1).getY());
                finalTransform.getMatrix(finalMatrix);
                dif = this.listPoints.get(this.listPoints.size()-1).getX() -x;
            }
            else{
                finalTransform.setToTranslation(this.listPoints.get(control).getX(), this.listPoints.get(control).getY());
                finalTransform.getMatrix(finalMatrix);
                dif = this.listPoints.get(control).getX() -x;}
            finalTransform.rotate(Math.PI/4);
            finalTransform.scale(1.5, 1.5);
            finalTransform.getMatrix(finalMatrix);
        }
        else{
            int x=0;
            finalTransform.setToTranslation(this.listPoints.get(control).getX(), this.listPoints.get(control).getY());
            finalTransform.rotate(Math.PI/4);
            finalTransform.scale(1.5, 1.5);
            finalTransform.getMatrix(finalMatrix);
            if(control<this.listPoints.size()){
            x = this.listPoints.get(control).getX();}
            control+=div;
      
            if(control>=this.listPoints.size()){
                //dif = this.listPoints.size()-control;
                initialTransform.setToTranslation(this.listPoints.get(this.listPoints.size()-1).getX(), this.listPoints.get(this.listPoints.size()-1).getY());
                initialTransform.getMatrix(initialMatrix);
                dif = this.listPoints.get(this.listPoints.size()-1).getX() -x; 
            }
            else{
                initialTransform.setToTranslation(this.listPoints.get(control).getX(), this.listPoints.get(control).getY());
                initialTransform.getMatrix(initialMatrix);
                dif = this.listPoints.get(control).getX() -x; 
            }      
            
        }
          
          
          for(int k=0;k<doublesteps;k++){
            System.out.println(dif);
              if(flag){
                if((dif>=2)||(i!=this.n*this.secoes)){
                intermediateTransform = new AffineTransform(convexCombination(initialMatrix, finalMatrix, k / doublesteps));
                s = intermediateTransform.createTransformedShape(square2);
                g2d.fill(s);}
                
                sustain(70);
                clearWindow(g2d);

              }
            else{
                if((dif>=2)||(i!=this.n*this.secoes)){
                intermediateTransform = new AffineTransform(convexCombination( finalMatrix,initialMatrix, k / doublesteps));
                s = intermediateTransform.createTransformedShape(square2);
                g2d.fill(s);}
                
                sustain(70);
                clearWindow(g2d);

              }         
          }
          flag = !flag;

      }
      
  }
  
  private void clearWindow(Graphics g2d)
  {
      g2d.setColor(Color.WHITE);
      g2d.fillRect(0, 0,largura,altura);
      g2d.setColor(Color.BLACK);
  }
  
  
  public void paint(Graphics g)
  {
    
    //Controi a cena
    Graphics2D g2d = (Graphics2D) g;
    
    //Desfine tamanho da linha
    g2d.setStroke(new BasicStroke(3.0f));
    g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,RenderingHints.VALUE_ANTIALIAS_ON);
    
    //Primeiro ponto do desenho, com raio ajustado
    //g2d.drawLine(this.x0,this.y0,this.x0,this.y0);
   
    //Metodo que invoca o desenho do(s) circulo(s) através do parâmetros iniciais
    // Ponto inici = (x0,y0)
    // Numero de circulos = n
    // Numero de Secoes de divisoes dos pontos = s
    // Raio = r
    // Algoritmo utilizado foi BRESENHAM!!
    this.circleDraw(g2d);
    this.drawingSquare(g2d);
    this.setVisible(false);
    
  }



  public static void main(String[] argv)
  {
      int x0,y0,n,s;
      float raio;
      
      
      //Parametros de entrada
      n = 2;
      raio = 150;
      x0 = 300;
      y0 = 200;
      s=3;
      
      
      //Gera a janela
      Trabalho1 f = new Trabalho1(n,raio,x0,y0,s);
      //Seta um titulo para a janela
      f.setTitle("Trabalho 1 - Computação Gráfica");
      
      //Seta a localizacao da janela baseada no ponto inicial
      //f.setLocation(x0, y0);
      //Seta o tamanho ideal da janela
      
      
      f.setVisible(true);
  }

}


